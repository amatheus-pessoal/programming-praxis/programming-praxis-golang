FROM golang:latest

WORKDIR /go/src/programming-praxis-golang

COPY . .

RUN make test